function [ newImage ] = classMap( Image )
%classMap find the gray scale image of taken image
%   Detailed explanation goes here
M=8;
count=1;
list=zeros([M,1,3]);
[row, col, dim] = size(Image);
list(1,1,:)=Image(1,1,:);
check=[1,2,3];
flag=0;
for k=1:M
    for i=1:row
        for j=1:col
            if (Image(i,j,:)==list(k,1,:))
                newImage(i,j)=(256/M)*k;
            else
                for d=1:count
                    if (Image(i,j,:)==list(d,1,:))
                        flag=1;
                    end
                end
                if(count~=8 && flag~=1)
                    list(count+1,1,:)=Image(i,j,:);
                    count=count+1;
                end
            end
            flag=0;
        end
    end
end

newImage=uint8(newImage);
figure('name','class Map','numbertitle','off');
imagesc(newImage);
end

